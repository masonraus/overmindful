absl-py==0.5.0
certifi==2018.8.24
chardet==3.0.4
enum34==1.1.6
future==0.16.0
idna==2.7
mock==2.0.0
mpyq==0.2.5
numpy==1.15.2
pandas==0.23.4
pbr==4.2.0
pkg-resources==0.0.0
portpicker==1.2.0
protobuf==3.6.1
pygame==1.9.4
PySC2==2.0.1
python-dateutil==2.7.3
pytz==2018.5
requests==2.19.1
s2clientprotocol==4.6.1.68195.0
scipy==1.1.0
six==1.11.0
sk-video==1.1.10
urllib3==1.23
websocket-client==0.53.0
whichcraft==0.4.1
