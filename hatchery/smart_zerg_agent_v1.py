import random
import math
import os.path
from datetime import datetime

import numpy as np
import pandas as pd

from pysc2.agents import base_agent
from pysc2.env import sc2_env
from pysc2.lib import actions, features, units
from absl import app

# some easy macros to save my hands from Carpal Tunnel lol
_NO_OP = actions.FUNCTIONS.no_op
_SELECT_POINT = actions.FUNCTIONS.select_point
_TRAIN_OVERLORD = actions.FUNCTIONS.Train_Overlord_quick
_BUILD_SPAWNING_POOL = actions.FUNCTIONS.Build_SpawningPool_screen
_TRAIN_ZERGLING = actions.FUNCTIONS.Train_Zergling_quick
_SELECT_ARMY = actions.FUNCTIONS.select_army
_ATTACK_MINIMAP = actions.FUNCTIONS.Attack_minimap
_BUILD_ROACH_WARREN = actions.FUNCTIONS.Build_RoachWarren_screen
_TRAIN_ROACH = actions.FUNCTIONS.Train_Roach_quick
_TRAIN_DRONE = actions.FUNCTIONS.Train_Drone_quick
_BUILD_EXTRACTOR = actions.FUNCTIONS.Build_Extractor_screen
_HARVERST = actions.FUNCTIONS.Harvest_Gather_screen
_BUILD_HATCHERY = actions.FUNCTIONS.Build_Hatchery_screen
_TRAIN_QUEEN = actions.FUNCTIONS.Train_Queen_quick

_PLAYER_RELATIVE = features.SCREEN_FEATURES.player_relative.index
_UNIT_TYPE = features.SCREEN_FEATURES.unit_type.index
_PLAYER_ID = features.SCREEN_FEATURES.player_id.index

_PLAYER_SELF = 1
_PLAYER_HOSTILE = 4

_ZERG_HATCHERY = units.Zerg.Hatchery
_ZERG_LAIR = units.Zerg.Lair
_ZERG_HIVE = units.Zerg.Hive
_ZERG_DRONE = units.Zerg.Drone
_ZERG_OVERLORD = units.Zerg.Overlord
_ZERG_OVERSEER = units.Zerg.Overseer
_ZERG_SPAWNING_POOL = units.Zerg.SpawningPool
_ZERG_LARVA = units.Zerg.Larva
_ZERG_ROACH_WARREN = units.Zerg.RoachWarren
_ZERG_ROACH = units.Zerg.Roach
_ZERG_VESPENE = units.Zerg.Extractor
_ZERG_QUEEN = units.Zerg.Queen
_VESPENE_GEYSER = units.Neutral.VespeneGeyser
_MINERALS = units.Neutral.MineralField

_NOT_QUEUED = [0]
_QUEUED = [1]

_REWARD_DATA = "resources/reward_data.txt"
_PICKLED_QLEARNING = "resources/qtable.pkl"

#basic action definitions
ACTION_DO_NOTHING = "donothing"
ACTION_SELECT_DRONE = "selectdrone"
ACTION_SELECT_LARVA = "selectlarva"
ACTION_TRAIN_OVERLORD = "trainoverlord"
ACTION_BUILD_SPAWNING_POOL = "buildspawningpool"
ACTION_SELECT_HATCHERY = "selecthatchery"
ACTION_TRAIN_ZERGLING = "trainzergling"
ACTION_SELECT_ARMY = "selectarmy"
ACTION_ATTACK = "attack"
ACTION_BUILD_ROACH_WARREN = "buildroachwarren"
ACTION_TRAIN_ROACH = "trainroach"
ACTION_TRAIN_DRONE = "traindrone"
ACTION_BUILD_EXTRACTOR = "buildextractor"
ACTION_ASSIGN_TO_EXTRACTOR = "assigntoextractor"
ACTION_BUILD_HATCHERY = "buildhatchery"
ACTION_BUILD_QUEEN = "buildqueen"

#put them in a list~
smart_actions = [
        ACTION_DO_NOTHING,
        ACTION_SELECT_DRONE,
        ACTION_SELECT_LARVA,
        ACTION_TRAIN_OVERLORD,
        ACTION_BUILD_SPAWNING_POOL,
        ACTION_SELECT_HATCHERY,
        ACTION_TRAIN_ZERGLING,
        ACTION_SELECT_ARMY,
        ACTION_BUILD_ROACH_WARREN,
        ACTION_TRAIN_ROACH,
        ACTION_TRAIN_DRONE,
        ACTION_BUILD_EXTRACTOR,
        ACTION_ASSIGN_TO_EXTRACTOR,
#        ACTION_BUILD_HATCHERY,
        ACTION_BUILD_QUEEN,
    ]

dont_attack = [
        ACTION_SELECT_DRONE,
        ACTION_BUILD_ROACH_WARREN,
        ACTION_BUILD_SPAWNING_POOL,
    ]

for mm_x in range(0, 64):
    for mm_y in range(0, 64):
        if (mm_x + 1) % 16 == 0 and (mm_y + 1) % 16 == 0:
            smart_actions.append(ACTION_ATTACK + '_' + str(mm_x - 8) + '_' + str(mm_y - 8))
            dont_attack.append(ACTION_ATTACK + '_' + str(mm_x - 8) + '_' + str(mm_y - 8))




#Reward Time
KILL_UNIT_REW = 0.2
KILL_BUILDING_REW = 0.5

FIRST_TECH_STRUCT_REW = 0.5
ADDITIONAL_TECH_STRUCT_PUN = -0.3

VESPENE_GEYSER_REW = 0.6

BUILD_LING_REW = 0.01
BUILD_ROACH_REW = 0.03
BUILD_DRONE_REW = 0.1
BUILD_QUEEN_REW = 0.02

BAD_ATTACK_TIMING_PUN = -0.01

GOOD_SUPPLY_BALANCE_REW = 0.3
BAD_SUPPLY_BALANCE_PUN = -0.3


#Bounding for appropriate rewards
goodTechStructNum = 2
meaningfulSupplyGap = 15
mineralSpacing = 50
hatchSpacing = 20


class SmartAgent(base_agent.BaseAgent):
    def __init__(self):
        super(SmartAgent, self).__init__()
        
        self.qlearn = QLearningTable(actions=list(range(len(smart_actions))))

        self.prev_unit_killed_score = 0
        self.prev_building_killed_score = 0
        self.prev_misc_reward = 0

        self.total_reward_cumulative = 0
        self.prev_move = None

        self.previous_action = None
        self.previous_state = None

    def save_cumulative_reward(self):

        rd = open(_REWARD_DATA, "a")
        rd.write(str(self.total_reward_cumulative) + "\n")
        rd.close()

    def reset_cumulative_reward(self):
        self.total_reward_cumulative = 0

    def save_progress(self):
        self.qlearn.q_table.to_pickle(_PICKLED_QLEARNING)

    def transformDistance(self, x, x_distance, y, y_distance):
        if not self.base_top_left:
            return [x - x_distance, y - y_distance]
        
        return [x + x_distance, y - y_distance]

    def transformLocation(self, x, y):
        if not self.base_top_left:
            x = 64 - x
            y = 64 - y
            if x < 0:
                x = 0
            if y < 0:
                y = 0
            return [x, y]

        if x > 83:
            x = 83
        if y > 83:
            y = 83

        return [x, y]

    def calcDistance(self, x1, y1, x2, y2):
        sqX = (x1 - x2)**2
        sqY = (y1 - y2)**2
        return math.sqrt(sqX + sqY)

    def step(self, obs):
        super(SmartAgent, self).step(obs)

        player_y, player_x = (obs.observation.feature_minimap.player_relative == 
                                _PLAYER_SELF).nonzero()
        self.base_top_left = 1 if player_y.any() and player_y.mean() <= 31 else 0

        # define current state chunk
        overlord_count = len(self.get_units_by_type(obs, _ZERG_OVERLORD))
        spawningPool_count = len(self.get_units_by_type(obs, _ZERG_SPAWNING_POOL))

        supply_cap = obs.observation.player.food_cap
        supply_used = obs.observation.player.food_used

        killed_unit_score = obs.observation.score_cumulative[5]
        killed_building_score = obs.observation.score_cumulative[6]

        current_state = np.zeros(20)
        current_state[0] = overlord_count
        current_state [1] = spawningPool_count 
        current_state[2] = supply_cap 
        current_state[3] = supply_used

        #TODO
        #graph reward vs time run
        #flowchart of alg
        #what do I want the audience to learn?
        #generational snapshot videos

        #TODO
        #build another hatchery
        #build more drones
        

        hot_squares = np.zeros(16)
        enemy_y, enemy_x = (obs.observation.feature_minimap.player_relative == _PLAYER_HOSTILE).nonzero()
        for i in range (0, len(enemy_y)):
            y = int(math.ceil((enemy_y[i] + 1) / 16))
            x = int(math.ceil((enemy_x[i] + 1) / 16))
        
            hot_squares[((y - 1) * 4) + (x - 1)] = 1
        
        if not self.base_top_left:
            hot_squares = hot_squares[::-1]
        
        for i in range(0, 16):
            current_state[i + 4] = hot_squares[i]

        if self.previous_action is not None:
            self.prev_move = smart_actions[self.previous_action]
            reward = self.prev_misc_reward

            if smart_actions[self.previous_action] == ACTION_BUILD_SPAWNING_POOL:
                pools = self.get_units_by_type(obs, _ZERG_SPAWNING_POOL)
                if len(pools) <= goodTechStructNum and len(pools) > 0:
                    self.prev_misc_reward += FIRST_TECH_STRUCT_REW
                elif len(pools) > goodTechStructNum:
                    self.prev_misc_reward += ADDITIONAL_TECH_STRUCT_PUN

            if smart_actions[self.previous_action] == ACTION_BUILD_ROACH_WARREN:
                warrens = self.get_units_by_type(obs, _ZERG_ROACH_WARREN)
                if len(warrens) <= goodTechStructNum and len(warrens) > 0:
                    self.prev_misc_reward += FIRST_TECH_STRUCT_REW
                elif len(warrens) > goodTechStructNum:
                    self.prev_misc_reward += ADDITIONAL_TECH_STRUCT_PUN


            if killed_unit_score > self.prev_unit_killed_score:
                reward += KILL_UNIT_REW

            if killed_building_score > self.prev_building_killed_score:
                reward += KILL_BUILDING_REW

            self.total_reward_cumulative += reward
            self.qlearn.learn(str(self.previous_state), self.previous_action, reward, str(current_state))

            self.prev_misc_reward = 0

        #close current state chunk
        rl_action = self.qlearn.choose_action(str(current_state))
        smart_action = smart_actions[rl_action]
        #smart_action = smart_actions[random.randrange(0, len(smart_actions) - 1)]

        self.prev_unit_killed_score = killed_unit_score
        self.prev_building_killed_score = killed_building_score
        self.previous_state = current_state
        self.previous_action = rl_action

        x = 0
        y = 0

        #ADDING SMART ACTIONS:
        # Make MACROS for the command and any units affected by it.
        # Add a text form of the command to the smart actions.
        # Then, below:
        # if the smart action is the new action,
        # check if it is something you are able to do
        # then execute the command using anything you need to do so

        if '_' in smart_action:
            smart_action, x, y = smart_action.split('_')

        if smart_action == ACTION_DO_NOTHING:
            return _NO_OP()

        elif smart_action == ACTION_SELECT_DRONE:
            print("drone")
            drones = self.get_units_by_type(obs, _ZERG_DRONE)

            if len(drones) > 0:
                drone = random.choice(drones)

                return _SELECT_POINT("select", (drone.x, drone.y))

        elif smart_action == ACTION_SELECT_LARVA:
            print("larva")
            larvae = self.get_units_by_type(obs, _ZERG_LARVA)

            if len(larvae) > 0:
                larva = random.choice(larvae)

                return _SELECT_POINT("select_all_type", (larva.x, larva.y))

        elif smart_action == ACTION_TRAIN_OVERLORD:
            if _TRAIN_OVERLORD.id in obs.observation.available_actions:
                if current_state[2] - current_state[3] <= meaningfulSupplyGap:
                    #supply cap         supply used
                    self.prev_misc_reward += GOOD_SUPPLY_BALANCE_REW
                else:
                    self.prev_misc_reward += BAD_SUPPLY_BALANCE_PUN

                print("overlord")
                return _TRAIN_OVERLORD("now")

        elif smart_action == ACTION_BUILD_SPAWNING_POOL:
            if _BUILD_SPAWNING_POOL.id in obs.observation.available_actions:
                x = random.randint(0, 83)
                y = random.randint(0, 83)
                print("sp (" + str(x) + "," + str(y) + ")")
                return _BUILD_SPAWNING_POOL("now", (x,y))

        elif smart_action == ACTION_SELECT_HATCHERY:
            print("hatch")
            hatcheries = self.get_units_by_type(obs, _ZERG_HATCHERY)

            if len(hatcheries) > 0:
                hatch = random.choice(hatcheries)

                return _SELECT_POINT("select", (hatch.x, hatch.y))

        elif smart_action == ACTION_TRAIN_ZERGLING:
            if _TRAIN_ZERGLING.id in obs.observation.available_actions:
                self.prev_misc_reward += BUILD_LING_REW

                print("ling")
                return _TRAIN_ZERGLING("now")

        elif smart_action == ACTION_SELECT_ARMY:
            if _SELECT_ARMY.id in obs.observation.available_actions:
                print("army")
                return _SELECT_ARMY("select")

        elif smart_action == ACTION_ATTACK:
            if self.prev_move in dont_attack:
                print ("attack cancelled")
                self.prev_misc_reward += BAD_ATTACK_TIMING_PUN
                return _NO_OP()
            else:
                if _ATTACK_MINIMAP.id in obs.observation.available_actions: 
                    print("attack!!!")
                    return _ATTACK_MINIMAP("now", self.transformLocation(int(x), int(y)))
        
        elif smart_action == ACTION_BUILD_ROACH_WARREN:
            if _BUILD_ROACH_WARREN.id in obs.observation.available_actions:
                x = random.randint(0, 83)
                y = random.randint(0, 83)
                print("RW (" + str(x) + "," + str(y) + ")")
                return _BUILD_ROACH_WARREN("now", (x,y))

        elif smart_action == ACTION_TRAIN_ROACH:
            if _TRAIN_ROACH.id in obs.observation.available_actions:
                self.prev_misc_reward += BUILD_ROACH_REW
                print("ROACH")
                return _TRAIN_ROACH("now")

        elif smart_action == ACTION_TRAIN_DRONE:
            if _TRAIN_DRONE.id in obs.observation.available_actions:
                self.prev_misc_reward += BUILD_DRONE_REW
                print("BLD drone")
                return _TRAIN_DRONE("now")

        elif smart_action == ACTION_BUILD_EXTRACTOR:
            if _BUILD_EXTRACTOR.id in obs.observation.available_actions:
                if len(self.get_units_by_type(obs, _ZERG_VESPENE)) < (goodTechStructNum 
                        * len(self.get_units_by_type(obs, _ZERG_HATCHERY))):
                    print("EXTRACTOR")
                    hatcheries = self.get_units_by_type(obs, _ZERG_HATCHERY)

                    if len(hatcheries) > 0:
                        hatch = random.choice(hatcheries)
                        geysers = self.get_units_by_type(obs, _VESPENE_GEYSER)
                        dist = 10000
                        ideal = None
                        for geyser in geysers:
                            tryDist = self.calcDistance(hatch.x, hatch.y, geyser.x, geyser.y)
                            if tryDist < dist:
                                dist = tryDist
                                ideal = geyser

                        self.prev_misc_reward += VESPENE_GEYSER_REW
                        return _BUILD_EXTRACTOR("now", (ideal.x, ideal.y))

        elif smart_action == ACTION_ASSIGN_TO_EXTRACTOR:
            if _HARVERST.id in obs.observation.available_actions:
                print("HARVEST GAS")
                if len(self.get_units_by_type(obs, _ZERG_VESPENE)) > 0:
                    extractors = self.get_units_by_type(obs, _ZERG_VESPENE)
                    ext = random.choice(extractors)
                    return _HARVERST("now", (ext.x, ext.y))

        elif smart_action == ACTION_BUILD_HATCHERY:
            if _BUILD_HATCHERY.id in obs.observation.available_actions:
                print("NEW HATCH")
                
                minerals = self.get_units_by_type(obs, _MINERALS)
                hatch = random.choice(self.get_units_by_type(obs, _ZERG_HATCHERY))
                filteredMinerals = []
                for m in minerals:
                    if self.calcDistance(hatch.x, hatch.y, m.x, m.y) > mineralSpacing:
                        filteredMinerals.append(m)

                desiredField = random.choice(filteredMinerals)
                x = -1
                y = -1
                while x < 0 and x > 63 and y < 0 and y > 63:
                    x = random.randint(desiredField.x - hatchSpacing, desiredField.x + hatchSpacing)
                    y = random.randint(desiredField.y - hatchSpacing, desiredField.y + hatchSpacing)
                return _BUILD_HATCHERY("now", (x, y))

        elif smart_action == ACTION_BUILD_QUEEN:
            if _TRAIN_QUEEN.id in obs.observation.available_actions:
                print("queen")

                queens = self.get_units_by_type(obs, _ZERG_QUEEN)
                hatcheries = self.get_units_by_type(obs, _ZERG_HATCHERY)

                if len(queens) >= 2 * (len(hatcheries)):
                    self.prev_misc_reward += BUILD_QUEEN_REW

                return _TRAIN_QUEEN("now")


        return _NO_OP()

    # Check if a unit type is selected
    def unit_type_is_selected(self, obs, unit_type):
        if (len(obs.observation.single_select) > 0 and
                obs.observation.single_select[0].unit_type == unit_type):
            return True

        if (len(obs.observation.multi_select) > 0 and
                obs.observation.multi_select[0].unit_type == unit_type):
            return True

        return False

    # select all units of one type
    def get_units_by_type(self, obs, unit_type):
        return [unit for unit in obs.observation.feature_units
                if unit.unit_type == unit_type]

    # Check if an action is possible
    def can_do(self, obs, action):
        return action in obs.observation.available_actions


class QLearningTable:
    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9, e_greedy=0.9):
        self.actions = actions
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        if os.path.isfile(_PICKLED_QLEARNING):
            self.q_table = pd.read_pickle(_PICKLED_QLEARNING)
        else:
            self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)


    def choose_action(self, observation):
        self.check_state_exist(observation)

        if np.random.uniform() < self.epsilon:
            #choose best action
            state_action = self.q_table.ix[observation, :]

            #some actions have the same value
            state_action = state_action.reindex(np.random.permutation(state_action.index))

            action = state_action.idxmax()
        else:
            #choose random action
            action = np.random.choice(self.actions)


        return action



    def learn(self, s, a, r, s_):
        self.check_state_exist(s_)
        self.check_state_exist(s)

        q_predict = self.q_table.ix[s, a]
        q_target = r + self.gamma + self.q_table.ix[s_, :].max()

        #update
        self.q_table.ix[s, a] += self.lr * (q_target - q_predict)


    def check_state_exist(self, state):
        if state not in self.q_table.index:
            #append new state to q table
            self.q_table = self.q_table.append(pd.Series([0] * len(self.actions), 
                            index=self.q_table.columns, name=state))


# This bit actually runs the game when we execute this file
def main(unused_argv):
    agent = SmartAgent()
    start_time = datetime.now()

    try:
        while True:
            with sc2_env.SC2Env(
                    map_name="Simple64",
                    players=[sc2_env.Agent(sc2_env.Race.zerg),
                        sc2_env.Bot(sc2_env.Race.terran,
                            sc2_env.Difficulty.very_easy)],
                    agent_interface_format=features.AgentInterfaceFormat(
                        feature_dimensions=features.Dimensions(screen=84, minimap=64),
                        use_feature_units=True),
                    step_mul=16,
                    game_steps_per_episode=50000,
                    visualize=True) as env:

                agent.setup(env.observation_spec(), env.action_spec())

                timesteps = env.reset()
                agent.reset()

                while True:
                    step_actions = [agent.step(timesteps[0])]
                    if timesteps[0].last():

                        end_time = datetime.now()

                        print( "---------- Duration: {} ----------".format(end_time - start_time))

                        agent.save_cumulative_reward()
                        agent.reset_cumulative_reward()
                        agent.save_progress()
                        break
                    timesteps = env.step(step_actions)

    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    while True:
        try:
            app.run(main)
        except:
            pass

