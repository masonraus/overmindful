from pysc2.agents import base_agent
from pysc2.env import sc2_env
from pysc2.lib import actions, features, units
from absl import app
import random

class ZergAgent(base_agent.BaseAgent):
    def step(self, obs):
        super(ZergAgent, self).step(obs)

        # Check if there is a spawning pool and make one if there isnt
        spawning_pools = self.get_units_by_type(obs, units.Zerg.SpawningPool)
        if len(spawning_pools) == 0:
            if self.unit_type_is_selected(obs, units.Zerg.Drone):
                if self.can_do(obs, actions.FUNCTIONS.Build_SpawningPool_screen.id):
                    x = random.randint(0, 83)
                    y = random.randint(0, 83)
                    print("SP (" + str(x) + "," + str(y) + ")")

                    return actions.FUNCTIONS.Build_SpawningPool_screen("now", (x,y))

            # select all the drones
            drones = self.get_units_by_type(obs, units.Zerg.Drone)
            if len(drones) > 0:
                drone = random.choice(drones)

                return actions.FUNCTIONS.select_point("select_all_type", (drone.x,
                    drone.y))
        
        # With all the larva selected ...
        if self.unit_type_is_selected(obs, units.Zerg.Larva):
            #Constantly be building drones
            #TODO decide when to build drones and make sure the 
            #queue isnt all drones
            # do this by selecting the hatchery and checking its queue obs
            make_a_drone = self.steps
            if (self.can_do(obs, actions.FUNCTIONS.Train_Drone_quick.id) and 
                    make_a_drone % 3 == 0):
                #print( "I can make a drone " + str(make_a_drone))
                return actions.FUNCTIONS.Train_Drone_quick("now")                


            # Build an overlord if supply is low, or ...
            free_supply = (obs.observation.player.food_cap -
                        obs.observation.player.food_used)
            if free_supply <= 10: #TODO, update this to keep a meaningful supply gap
                if self.can_do(obs, actions.FUNCTIONS.Train_Overlord_quick.id):
                    return actions.FUNCTIONS.Train_Overlord_quick("now")
            
            # Make some Zerglings if it isnt!
            if self.can_do(obs, actions.FUNCTIONS.Train_Zergling_quick.id):
                return actions.FUNCTIONS.Train_Zergling_quick("now")

        # select all the larvae
        larvae = self.get_units_by_type(obs, units.Zerg.Larva)
        if len(larvae) > 0:
            larva = random.choice(larvae)

            return actions.FUNCTIONS.select_point("select_all_type", (larva.x,
                                                                    larva.y))


        return actions.FUNCTIONS.no_op()

    # Check if a unit type is selected
    def unit_type_is_selected(self, obs, unit_type):
        if (len(obs.observation.single_select) > 0 and 
                obs.observation.single_select[0].unit_type == unit_type):
            return True

        if (len(obs.observation.multi_select) > 0 and 
                obs.observation.multi_select[0].unit_type == unit_type):
            return True
        
        return False

    # select all units of one type
    def get_units_by_type(self, obs, unit_type):
        return [unit for unit in obs.observation.feature_units
                if unit.unit_type == unit_type]

    # Check if an action is possible
    def can_do(self, obs, action):
        return action in obs.observation.available_actions

# This bit actually runs the game when we execute this file
def main(unused_argv):
    agent = ZergAgent()

    try:
        while True:
            with sc2_env.SC2Env(
                map_name="AbyssalReef",
                players=[sc2_env.Agent(sc2_env.Race.zerg),
                    sc2_env.Bot(sc2_env.Race.terran,
                        sc2_env.Difficulty.very_easy)],
                agent_interface_format=features.AgentInterfaceFormat(
                    feature_dimensions=features.Dimensions(screen=84, minimap=64),
                    use_feature_units=True),
                step_mul=16,
                game_steps_per_episode=4000,
                visualize=True) as env:

                agent.setup(env.observation_spec(), env.action_spec())

                timesteps = env.reset()
                agent.reset()

                while True:
                    step_actions = [agent.step(timesteps[0])]
                    if timesteps[0].last():
                        break
                    timesteps = env.step(step_actions)

    except KeyboardInterrupt:
        pass

if __name__ == "__main__":
    app.run(main)
