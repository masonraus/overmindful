Last week I planned on working on:
* Researching how other Zerg AIs play and win the game.
* Researching how to use a ML library
* Getting my AI to constantly build workers and have them mine
* POTENTIALLY get workers to build a gas extractor and mine from that after a certain time or supply count

Instead, I spent some time learning the library in general by making a basic scripted AI that:
* builds workers
* builds a spawning pool
* builds overlords to keep supply avalible
* and builds zerglings so it can attack the enemy (but it doesnt attack yet)

This week I will be spending time on 
* Researching how other Zerg AIs play and win the game.
* Researching how to use an ML library
* POTENTIALLY Begin developing my ML algorithm so I can start training my AI

