Oh Boy.
Tasks I WAS planning on working on for the last week were 
* Spending some time doing tutorials for numpy
* applying those tutorials in tutorials for openCV
* Looking at possible UI tools for the interface

HOWEVER!

Last week, I instead switched my project, crafted a new "skinny" project proposal, created a bunch of user stories on scrumdesk, broke, rebuilt, and FINALLY MADE A BACKUP of my VM, and got everything working using PySc2 provided test agents. 

This week I plan to work on
* Researching how other Zerg AIs play and win the game.
* Researching how to use a ML library
* Getting my AI to constantly build workers and have them mine
* POTENTIALLY get workers to build a gas extractor and mine from that after a certain time or supply count
