#!/home/mason/overmindful/.venv/bin/python
# use this to create a menu for easy execution of common training tasks
import os
import sys

_PYTHON = "/home/mason/overmindful/.venv/bin/python"

argv = sys.argv

if len(argv) <= 1:

    print ("Overmind-ful Menu:\n")

    print ("0: Run Agent v Agent demo")
    print ("1: Run Scripted_Zerg_Agent")
    print ("2: Run Smart_Zerg_Agent_v1")

    print ("\n")

    choice = int(input ("What are you trying to do?\t"))

else:
    choice = int(argv[1])

if choice == 0:
    os.system(_PYTHON + " -m pysc2.bin.agent --map Simple64 --agent2 pysc2.agents.random_agent.RandomAgent")

elif choice == 1:
    os.system(_PYTHON + " hatchery/scripted_zerg_agent.py")

elif choice == 2:
    os.system(_PYTHON + " hatchery/smart_zerg_agent_v1.py")

